﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioFire : MonoBehaviour
{

    public AudioClip fireSound;
    AudioSource soundSource;
    public bool alreadyPlayed = false;
    public float Volume;

    // Start is called before the first frame update
    void Start()
    {
        soundSource = GetComponent<AudioSource>();
    }
    //// Update is called once per frame
    //void Update()
    //{
    //    if (DragonSript.distance >= distanceThreshold)
    //    {


    //        anim.SetBool("isFlying", false);

    //    }
    //    else if (distance <= distanceThreshold)
    //    {
    //        anim.SetBool("isFlying", true);
    //        soundSource.Play();

    //    }
    //}

    void OnTriggerEnter()
    {
        if(!alreadyPlayed)
        {
            soundSource.PlayOneShot(fireSound, Volume);
            alreadyPlayed = true;
        }
    }

}