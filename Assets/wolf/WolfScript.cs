﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfScript : MonoBehaviour
{
    public GameObject Wolf;
    public GameObject Dragon;
    public float distance;
    public float distanceRange;
    public Animator Wanim;


    // Start is called before the first frame update
    void Start()
    {
        Wanim = GetComponent<Animator>();
        Wanim.SetBool("isRunning", false);
    }

    // Update is called once per frame
    void Update()
    {

        //si les objets dragon et wolf sont detectés
        if (Dragon != null && Wolf != null)
        {
            //calculer la distance entre le dragon et le Loup
            distance = Vector3.Distance(Dragon.transform.position, Wolf.transform.position);
            //distance minimum pour lancer l'attaque de dragon 
            distanceRange = 3.0f;

            if (distance >= distanceRange)
            {
                // appel a la methode qui fait tourner le Lou face au Dragon
                Lookat();
                //
                Wanim.SetBool("isRunning", false);
            }
            else if (distance <= distanceRange)
            {
                //appale a la methode qui fait tourner le lou Contre le Dragon pour fuire 
                Runout();
                // lancer l'annimation isRunning de lou 
                Wanim.SetBool("isRunning", true);
            }


        }
        else
        {
            Reset();

        }
    }

    //la methode qui fait tourner le Lou face au Dragon
    void Lookat()
    {
        Wolf.transform.LookAt(Dragon.transform);

    }
    //methode qui fait tourner le Lou contre le Dragon 
    void Runout()
   
        {
            Wolf.transform.rotation = Quaternion.LookRotation(Wolf.transform.position - Dragon.transform.position);
        }

    void Reset()
    {
        Wolf.transform.localEulerAngles = new Vector3(0, 0, 0);
        Wanim.SetBool("isRunning", false);

    }
}
