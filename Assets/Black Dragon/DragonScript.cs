﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonScript : MonoBehaviour
{
    public GameObject Wolf;
    public GameObject Dragon;
    public float distance;
    public float distanceRange;
    public Animator anim;
    public AudioClip fireSound;
    AudioSource soundSource;
    public bool alreadyPlayed = false;
    public float Volume = 1f;
    public ParticleSystem particleLauncher;




    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        // le parametre d'annimator controler isFlying set to false 
        anim.SetBool("isFlying", false);
        soundSource = GetComponent<AudioSource>();

        // pas d'emission de particuel de feu 
        particleLauncher.Emit(0);

        // le son du'attack dragon et en pause a l état initial
        soundSource.Pause();

    }

    // Update is called once per frame
    void Update()
    {

        //si les objets dragon et wolf sont detectés
        if (Dragon != null && Wolf != null)
        {
            //calculer la distance entre le dragon et le Lou
            distance = Vector3.Distance(Dragon.transform.position, Wolf.transform.position);
            distanceRange = 3.0f;

            // comparaison de la distance avec la distance range deja pricisé pour lancer l'attack = 2.5.
            if (distance >= distanceRange)
            {
               // le dragon se tourne face au lou
                 Lookat();

                //
                anim.SetBool("isFlying", false);

                soundSource.Pause();
                // set le son est ce qu'il est déja lancé ou non 
                alreadyPlayed = false;
                // arreter le feu du dragon 
                particleLauncher.Emit(0);
            }
                    // la condition pour lancer l'attack dragon 
            else if(distance <= distanceRange && distance > 0)
            {
                // lancer l'annimation d'attack fly du dragon 
                anim.SetBool("isFlying", true);

                // lancer le feu du dragon 
                particleLauncher.Emit(1);

                // lancer le son d'attack dragon 
                if (!alreadyPlayed)
                {
                    // lancer le son d'attack dragon 
                    soundSource.Play();

                    alreadyPlayed = true;
                
                }

            }

        }
        else
        {
            Reset();

        }
    }


    void Lookat()
    {
        Dragon.transform.LookAt(Wolf.transform);

    }
        void Reset()
        {
            Dragon.transform.localEulerAngles = new Vector3(0, 0, 0);
        anim.SetBool("isFlying", false);

    }
        

}
