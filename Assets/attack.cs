﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class attack : MonoBehaviour
{
    //StateMachineBehaviour, 
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
    public GameObject Dragon;
    public GameObject Wolf;
    public float distance;
    public float distanceThreshold;
    public bool IsFlying = false;
    public bool IsRuning = false;
    AudioSource sound;

    void CheckDistance()
    {
        if (Dragon != null && Wolf != null)
        { // if both ninja cards are tracked
            distance = Vector3.Distance(Dragon.transform.position, Wolf.transform.position);

            //if (distance <= distanceThreshold)
            Lookat();

                //Debug.Log("FIGHT" + swordsCard + " " + starCard);
                Dragon.GetComponent<Animator>().speed = 1.25f;
                Wolf.GetComponent<Animator>().speed = 1.25f;

                    Debug.Log("Armature|Fly_New");
                    Dragon.GetComponent<Animator>().Play("Armature|Fly_New", -1, 0); // attack without weapons
                    Wolf.GetComponent<Animator>().Play("Wolf_Skeleton|Wolf_Run_Cycle_", -1, 0);
                    //DragonSound();
            SetBools();
            IsFlying = true;
            IsRuning = true;
        }
        }


    void Lookat()
    {
        Dragon.transform.LookAt(Wolf.transform);
        Wolf.transform.LookAt(Dragon.transform);
        Dragon.transform.localEulerAngles = new Vector3(0, Dragon.transform.localEulerAngles.y, 0);
        Wolf.transform.localEulerAngles = new Vector3(0, Wolf.transform.localEulerAngles.y, 0);
    }
    void SetBools()
    {

        IsFlying = true;
        IsRuning = true;
        //onceAttack = true;
        //onceIdle = true;
        //onceDance = true;
    }
    //void DragonSound()
    //{
    //    sound = Dragon.GetComponent<AudioSource>();
    //    sound.clip = Dragon.
    //    sound.Play();
    //}
}


